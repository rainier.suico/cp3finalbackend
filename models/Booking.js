const mongoose = require("mongoose");

const BookingSchema = mongoose.Schema({
  date_time: {
    type: String,
    required: true
  },
  name: {
    type: String,
    default: ''
  },
  status: {
    type: String,
    default: "pending"
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'user'
  }
});

module.exports = mongoose.model("booking", BookingSchema);
